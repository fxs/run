#!/bin/sh

# Clean dependencies
#
clean_mod() {
    go mod tidy
}

# Download dependencies
#
get_mod() {
    go mod download
}

# Check outdated dependencies
#
check_mod_outdated() {
    go list -u -f '{{if (and (not (or .Main .Indirect)) .Update)}}{{.Path}}: {{.Version}} -> {{.Update.Version}}{{end}}' -m all 2> /dev/null
}

# Check formated code
#
check_format() {
    if [ -n "$(gofmt -l .)" ]; then
        echo "Go code is not formatted:"
        gofmt -d .
        exit 1
    fi
    go vet ./...
#    /go/bin/golint ./...
}

# Run unit test
#
unit_test() {
    go test ./... -cover
}

# Run unit test with details
#
unit_test_verbose() {
    go test ./... -v -cover | sed ''/PASS/s//$(printf "\033[32mPASS\033[0m")/'' | sed ''/FAIL/s//$(printf "\033[31mFAIL\033[0m")/''
}

# Generate code coverage
#
coverage_unit_test() {
    go test -race -cover ./cmd/... ./internal/... -coverprofile=cover.out -covermode=atomic
}

# Display code coverage
#
coverage_display() {
    go tool cover -func "cover.out"
}

# Build for local architecture
#
build_local() {
    env GOOS=linux GOARCH=amd64 go build -ldflags="-X 'main.Version=LOCAL' -s -w" -o run_local_linux_amd64 ./cmd/run
    env GOOS=darwin GOARCH=amd64 go build -ldflags="-X 'main.Version=LOCAL' -s -w" -o run_local_darwin_amd64 ./cmd/run
}

# Run local Robot Framework tests
#
rf_tests() {
    mkdir -p reports
	if [ -z "${container}" ]; then
		chmod 777 reports
		docker run -t --rm -v "${PWD}":/app jfxs/robot-framework:latest robot --outputdir /app/reports -v SCRIPT:/app/run_local_linux_amd64 -v TESTS_DIR_PATH:/app/tests -v VERSION:LOCAL /app/tests/RF
		chmod 755 reports
	else
		robot --outputdir reports -v SCRIPT:./run_local_linux_amd64 -v VERSION:LOCAL tests/RF
	fi

}