** Settings ***
Documentation     Target name and description tests
...
...               Tests to check target name and description.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
Simple target name
    [Tags]    name    sanity
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Hello, world!

Simple target name on file 2
    [Tags]    name    sanity
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world2
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Hello, world2!

Target name with blank space for function signature
    [Tags]    name
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world_with_blank
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Hello, world with blank!

Simple description of a target
    [Tags]    description    sanity
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    A target to display "Hello, world!"

Multiline description of a target
    [Tags]    description
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    A target to display "Hello, world!" with blank space for function signature
    And Should Contain    ${result.stdout}    with multiline comment.

Target with no description
    [Tags]    description
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1 
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    overrided_hello_world
    And Should Not Contain    ${result.stdout}    hello_world_with_no_desc

Target with an error
    [Tags]    execution
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world_with_error
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stdout}    Hello, world with an error!
    And Should Contain   ${result.stderr}    An error in stderr
    And Should Contain   ${result.stderr}    exit status 10

Target not found
    [Tags]    execution
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    wrong_target
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain   ${result.stderr}    target not found "wrong_target"
