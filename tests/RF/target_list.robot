** Settings ***
Documentation     List target tests
...
...               Tests to check the content of the list of target.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
[-d] option define run directory
    [Tags]    list    sanity
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1 
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    10-hello-world
    And Should Contain    ${result.stdout}    hello_world
    And Should Contain    ${result.stdout}    A target to display "Hello, world!"
    And Should Contain    ${result.stdout}    A target to display "Hello, world!" with blank space for function signature
    And Should Contain    ${result.stdout}    with multiline comment.
    And Should Contain    ${result.stdout}    20-hello-world
    And Should Contain    ${result.stdout}    hello_world2
    And Should Contain    ${result.stdout}    A target to display "Hello, world2!"
