** Settings ***
Documentation     Run directory option tests
...
...               Tests to check run directory option.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
[-d] option define run directory
    [Tags]    directory    sanity
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1 
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    10-hello-world
    And Should Contain    ${result.stdout}    hello_world

[-d] option with inexistant directory
    [Tags]    directory
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/wrongDir
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    wrongDir: no such file or directory

[-d] option with directory with no scripts
    [Tags]    directory
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/RF
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    core.robot

[--dir] option define run directory
    [Tags]    directory    sanity
    ${result} =    When Run Process    ${SCRIPT}    --dir    ${TESTS_DIR_PATH}/files/run1 
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    10-hello-world
    And Should Contain    ${result.stdout}    hello_world

[--dir] option with inexistant directory
    [Tags]    directory
    ${result} =    When Run Process    ${SCRIPT}    --dir    ${TESTS_DIR_PATH}/files/wrongDir
    Then Should Be Equal As Integers    ${result.rc}    1
    And Should Contain    ${result.stderr}    wrongDir: no such file or directory

[--dir] option with directory with no scripts
    [Tags]    directory
    ${result} =    When Run Process    ${SCRIPT}    --dir    ${TESTS_DIR_PATH}/RF
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    core.robot
