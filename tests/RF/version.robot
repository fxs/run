** Settings ***
Documentation     Version command tests
...
...               Tests to check version command.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
[-v] option should show version
    [Tags]    version    sanity
    ${result} =    When Run Process    ${SCRIPT}    -v
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    run version ${VERSION}

[--version] option should show version
    [Tags]    version
    ${result} =    When Run Process    ${SCRIPT}    --version
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    run version ${VERSION}
