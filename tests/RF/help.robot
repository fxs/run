** Settings ***
Documentation     Help command tests
...
...               Tests to check help command.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
[-h] option should show help
    [Tags]    help    sanity
    Check help    ${SCRIPT}    -h

[--help] option should show help
    [Tags]    help
    Check help    ${SCRIPT}    --help

[h] option should show help
    [Tags]    help
    Check help    ${SCRIPT}    h

[help] option should show help
    [Tags]    help
    Check help    ${SCRIPT}    help

[help help] option should show help for help command
    [Tags]    help    sanity
    ${result} =    When Run Process    ${SCRIPT}    help    help
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    - Shows a list of commands or help

[help] for a target with no options and arguments
    [Tags]    help
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world    -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    NAME:
    And Should Contain    ${result.stdout}    A target to display "Hello, world!"
    And Should Contain    ${result.stdout}    USAGE:
    And Should Contain    ${result.stdout}    hello_world [command options] [arguments...]
    And Should Contain    ${result.stdout}    CATEGORY:
    And Should Contain    ${result.stdout}    10-hello-world
    And Should Contain    ${result.stdout}    OPTIONS:
    And Should Contain    ${result.stdout}    --help, -h  show help

[help] for a target not in first category with no options and arguments
    [Tags]    help
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    hello_world2    -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    NAME:
    And Should Contain    ${result.stdout}    A target to display "Hello, world2!"
    And Should Contain    ${result.stdout}    USAGE:
    And Should Contain    ${result.stdout}    hello_world2 [command options] [arguments...]
    And Should Contain    ${result.stdout}    CATEGORY:
    And Should Contain    ${result.stdout}    20-hello-world2
    And Should Contain    ${result.stdout}    OPTIONS:
    And Should Contain    ${result.stdout}    --help, -h  show help

*** Keywords ***
Check help
    [Arguments]    ${script}    ${arg}
    ${result} =    When Run Process    ${script}    ${arg}
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    USAGE:
    And Should Contain    ${result.stdout}    help, h  Shows a list of commands
    And Should Contain    ${result.stdout}    --dir value, -d value
