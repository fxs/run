** Settings ***
Documentation     List target tests
...
...               Tests to check the content of the list of target.

Library           Process
Resource          common.robot

*** Variables ***

*** Test Cases ***
Overrided target has parenthesis
    [Tags]    list
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1 
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    (overrided_hello_world)
    And Should Contain    ${result.stdout}    An initial target
    And Should Contain    ${result.stdout}    ${SPACE}overrided_hello_world${SPACE}
    And Should Contain    ${result.stdout}    An overrided target.

Description of an overrided target
    [Tags]    description
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    An initial target
    And Should Contain    ${result.stdout}    An overrided target

Execution of an overrided target
    [Tags]    execution
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    overrided_hello_world
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    Hello, world overrided!

[help] for an overrided target
    [Tags]    help
    ${result} =    When Run Process    ${SCRIPT}    -d    ${TESTS_DIR_PATH}/files/run1    overrided_hello_world    -h
    Then Should Be Equal As Integers    ${result.rc}    0
    And Should Contain    ${result.stdout}    NAME:
    And Should Contain    ${result.stdout}    An overrided target.
    And Should Contain    ${result.stdout}    USAGE:
    And Should Contain    ${result.stdout}    overrided_hello_world [command options] [arguments...]
    And Should Contain    ${result.stdout}    CATEGORY:
    And Should Contain    ${result.stdout}    20-hello-world2
    And Should Contain    ${result.stdout}    OPTIONS:
    And Should Contain    ${result.stdout}    --help, -h  show help
