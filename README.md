# jfxs / run

[![Software License](https://img.shields.io/badge/license-GPL%20v3-informational.svg?style=flat&logo=gnu)](LICENSE)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/fxs/run)](https://goreportcard.com/report/gitlab.com/fxs/run)
[![Pipeline Status](https://gitlab.com/fxs/run/badges/master/pipeline.svg)](https://gitlab.com/fxs/run/pipelines)
[![codecov](https://codecov.io/gl/fxs/run/branch/master/graph/badge.svg?token=3A575QO2WU)](https://codecov.io/gl/fxs/run)
[![Robot Framework Tests](https://fxs.gitlab.io/run/all_tests.svg)](https://fxs.gitlab.io/run/report.html)
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)

run is a lightweight tool to unify shell scripts execution.

## Find Us

* [Gitlab](https://gitlab.com/fxs/run)

## Authors

* **FX Soubirou** - *Initial work* - [Gitlab repositories](https://gitlab.com/users/fxs/projects)

## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See the [LICENSE](https://gitlab.com/fxs/docker-ci-toolkit/blob/master/LICENSE) file for details.
