/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2021 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"bytes"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInitVersionShort(t *testing.T) {
	os.Args = []string{"./run", "-v"}
	out = new(bytes.Buffer)
	main()
	output := out.(*bytes.Buffer).String()
	assert.Equal(t, "run version DEV\n", output)
}

func TestInitVersion(t *testing.T) {
	os.Args = []string{"./run", "--version"}
	out = new(bytes.Buffer)
	main()
	output := out.(*bytes.Buffer).String()
	assert.Equal(t, "run version DEV\n", output)
}

func TestInitHelpShort1(t *testing.T) {
	os.Args = []string{"./run", "h"}
	out = new(bytes.Buffer)
	main()
	output := out.(*bytes.Buffer).String()
	assert.Contains(t, "NAME:", output)
	assert.Contains(t, "--help, -h", output)
}

func TestInitHelpShort2(t *testing.T) {
	os.Args = []string{"./run", "-h"}
	out = new(bytes.Buffer)
	main()
	output := out.(*bytes.Buffer).String()
	assert.Contains(t, "NAME:", output)
	assert.Contains(t, "--help, -h", output)
}
