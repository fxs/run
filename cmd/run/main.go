/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package main

import (
	"fmt"
	"io"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/fxs/run/internal/handler"
)

// Version Version of run
var Version string = "DEV"

// AuthorName Name of the author
var AuthorName string = "FX Soubirou"

// AuthorEmail Email of the author
var AuthorEmail string = "soubirou@yahoo.fr"

// runDir Structure of the directory of the run scripts
var runDir *handler.Directory

// out Default out
var out io.Writer = os.Stdout

func main() {

	initRunDir()

	app := &cli.App{
		Name:    "run",
		Version: Version,
		Authors: []*cli.Author{
			{
				Name:  AuthorName,
				Email: AuthorEmail,
			},
		},
		Usage:     "a tool to unify shell scripts execution",
		UsageText: "run [global options] command [arguments...]",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    "dir",
				Aliases: []string{"d"},
				Value:   "./.run.d",
				Usage:   "Full path of run scripts directory",
			},
		},
		Action: func(c *cli.Context) error {
			if c.NArg() > 0 {
				var target string = c.Args().Get(0)
				fmt.Fprintf(os.Stderr, "run: target not found \"%v\"\n", target)
				os.Exit(1)
			}
			_, err := io.WriteString(out, runDir.FormatHelps())
			if err != nil {
				fmt.Fprintf(os.Stderr, "run: %v\n", err)
				os.Exit(1)
			}
			return nil
		},
		Commands: runDir.FilesFunctionsToCommands(),
	}

	cli.VersionPrinter = func(c *cli.Context) {
		fmt.Fprintf(out, "run version %s\n", c.App.Version)
	}

	err := app.Run(os.Args)
	if err != nil {
		fmt.Fprintf(os.Stderr, "run: %v\n", err)
		os.Exit(1)
	}
}

func initRunDir() {

	var isRunDirNecessary = true

	if (len(os.Args) > 1) &&
		((os.Args[1] == "-v") || (os.Args[1] == "--version") ||
			(os.Args[1] == "-h") || (os.Args[1] == "--help") || (os.Args[1] == "h") || (os.Args[1] == "help")) {
		isRunDirNecessary = false
	}

	var err error
	if (len(os.Args) > 2) && ((os.Args[1] == "-d") || (os.Args[1] == "--dir")) {
		runDir = handler.NewDirectoryWithPath(os.Args[2])
	} else {
		runDir, err = handler.NewDirectory()
		if err != nil {
			fmt.Fprintf(os.Stderr, "run: %v\n", err)
			os.Exit(1)
		}
	}

	var runDirExist bool
	runDirExist, err = runDir.IsExist()
	if !runDirExist && isRunDirNecessary {
		fmt.Fprintf(os.Stderr, "run: %v\n", err)
		os.Exit(1)
	}

	if runDirExist {
		err = runDir.GetFiles()
		if (err != nil) && isRunDirNecessary {
			fmt.Fprintf(os.Stderr, "run: %v\n", err)
			os.Exit(1)
		}
		if err != nil {
			fmt.Fprintf(out, "run: [Warning] %v\n", err)
		}
	}
}
