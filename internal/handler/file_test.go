/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewFile(t *testing.T) {
	path := "testdata/file1"
	f, err := NewFile(path)
	assert.Nil(t, err)
	assert.Equal(t, path, f.Path)
	assert.Equal(t, "A sample content\n", f.Content)
	assert.Equal(t, 0, len(f.Functions))
}

func TestNewFileError(t *testing.T) {
	path := "testdata/wrongFile"
	f, err := NewFile(path)
	assert.Error(t, err)
	assert.Equal(t, path, f.Path)
}

func TestGetTitle(t *testing.T) {
	f := File{
		Path: "/directory/myFile",
	}
	assert.Equal(t, "myFile", f.GetTitle())
}

func TestGetTitleWithPrefix(t *testing.T) {
	f := File{
		Path: "/directory/10-myFile",
	}
	assert.Equal(t, "10-myFile", f.GetTitle())
}

func TestLoad(t *testing.T) {
	f := File{
		Path: "testdata/file1",
	}
	err := f.load()
	assert.Nil(t, err)
	assert.Equal(t, "A sample content\n", f.Content)
}

func TestLoadError(t *testing.T) {
	f := File{
		Path: "testdata/wrongFile",
	}
	err := f.load()
	assert.Error(t, err)
}

func TestParseFunctionsNoFunction1(t *testing.T) {
	f := File{
		Path: "testdata/file1",
	}
	err := f.load()
	assert.Nil(t, err)
	f.parseFunctions()
	assert.Equal(t, 0, len(f.Functions))
}

func TestParseFunctionsNoFunction2(t *testing.T) {
	f := File{
		Path: "testdata/file2",
	}
	err := f.load()
	assert.Nil(t, err)
	f.parseFunctions()
	assert.Equal(t, 0, len(f.Functions))
}

func TestParseFunctions(t *testing.T) {
	f := File{
		Path: "testdata/file3",
	}
	err := f.load()
	assert.Nil(t, err)
	f.parseFunctions()
	assert.Equal(t, 3, len(f.Functions))
}

func TestFormatHelpsEmpty(t *testing.T) {
	f, _ := NewFile("testdata/file1")
	expected := fmt.Sprintf(formatedHelpTitle, "file1")
	assert.Equal(t, expected, f.FormatHelps())
}

func TestFormatHelpsThreeFunctions(t *testing.T) {
	f, _ := NewFile("testdata/file3")
	expected1 := fmt.Sprintf(formatedHelpTitle, "file3")
	expected2 := fmt.Sprintf(formatedHelpLine+formatedHelpLine, f.Functions[0].Name, f.Functions[0].Description[0], "", f.Functions[0].Description[1])
	expected3 := fmt.Sprintf(formatedHelpLine+formatedHelpLine, f.Functions[1].Name, f.Functions[1].Description[0], "", f.Functions[1].Description[1])
	expected4 := fmt.Sprintf(formatedHelpLine, f.Functions[2].Name, f.Functions[2].Description[0])
	assert.Equal(t, expected1+expected2+expected3+expected4, f.FormatHelps())
}

func TestFunctionsToCommands(t *testing.T) {
	f, _ := NewFile("testdata/file3")
	c := f.FunctionsToCommands("Content")
	assert.Equal(t, "hello_world", c[0].Name)
	assert.Equal(t, "file3", c[0].Category)
	assert.Equal(t, "A target to display \"Hello, world!\" for function signature with multiline comment.", c[0].Usage)
	assert.Equal(t, "hello_world_with_blank", c[1].Name)
	assert.Equal(t, "file3", c[1].Category)
	assert.Equal(t, "A target to display \"Hello, world!\" with blank space for function signature with multiline comment.", c[1].Usage)
}
