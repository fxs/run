/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/urfave/cli/v2"
)

// formatedHelpLine Line formated for help function
const formatedHelpTitle = "\033[0;33m%s\033[0m\n"

// File file structure
type File struct {
	Path      string
	Content   string
	Functions []*Function
}

// NewFile file constructor
func NewFile(path string) (*File, error) {

	f := new(File)
	f.Path = path
	err := f.load()
	if err == nil {
		f.parseFunctions()
	}
	return f, err
}

// GetTitle get basename of file without digit at the beginning if any
func (f *File) GetTitle() string {

	return filepath.Base(f.Path)
}

// load load the content of the file
// Return Error if any
func (f *File) load() error {

	data, err := ioutil.ReadFile(f.Path)
	if err == nil {
		f.Content = string(data)
	}
	return err
}

// parseFunctions get function, arguments, description from content of file
func (f *File) parseFunctions() {

	re := regexp.MustCompile(`^ *(.+) *\(\) *{ *`)

	workFunction := Function{}

	for _, line := range strings.Split(strings.TrimSuffix(f.Content, "\n"), "\n") {

		// if description of function
		if strings.HasPrefix(line, "#") {
			// Remove prefix and spaces
			desc := strings.TrimSpace(strings.TrimPrefix(line, "#"))
			if len(desc) > 0 {
				workFunction.Description = append(workFunction.Description, desc)
			}
		} else {
			// Not comment, is previous line description ?
			if workFunction.HasDescription() {
				// Is signature of function
				if re.MatchString(line) {
					function := workFunction.Copy()

					function.Name = strings.TrimSpace(re.FindStringSubmatch(line)[1])
					f.Functions = append(f.Functions, function)
				}
			}
			workFunction.Reset()
		}
	}
}

// FormatHelps Format all functions helps
func (f *File) FormatHelps() string {

	helps := []string{fmt.Sprintf(formatedHelpTitle, f.GetTitle())}

	for _, function := range f.Functions {
		helps = append(helps, fmt.Sprint(function.FormatHelp()))
	}
	return strings.Join(helps, "")
}

// FunctionsToCommands convert functions to cli commands
func (f *File) FunctionsToCommands(content string) []*cli.Command {

	commands := []*cli.Command{}
	for _, function := range f.Functions {
		if !function.IsOverrided {
			command := function.toCommand(f.GetTitle(), content)
			commands = append(commands, &command)
		}
	}
	return commands
}
