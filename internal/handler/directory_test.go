/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2021 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewDirectory(t *testing.T) {
	d, err := NewDirectory()
	assert.Nil(t, err)
	dir, _ := os.Getwd()
	assert.Equal(t, dir+"/"+defaultRunDir, d.FullPath)
}

func TestNewDirectoryWithPath(t *testing.T) {
	dir := "./testdata"
	d := NewDirectoryWithPath(dir)
	assert.Equal(t, dir, d.FullPath)
}

func TestIsExistTrue(t *testing.T) {
	d := Directory{
		FullPath: "./testdata",
	}
	exist, err := d.IsExist()
	assert.Nil(t, err)
	assert.Equal(t, exist, true)
}

func TestIsExistFalse(t *testing.T) {
	d := Directory{
		FullPath: "./wrongDir",
	}
	exist, err := d.IsExist()
	assert.NotNil(t, err)
	assert.Equal(t, exist, false)
}

func TestGetFiles(t *testing.T) {
	d := Directory{
		FullPath: "./testdata",
	}
	err := d.GetFiles()
	assert.Nil(t, err)
	assert.Equal(t, 4, len(d.Files))
	assert.Equal(t, "file1", d.Files[0].GetTitle())
}

func TestGetFilesError(t *testing.T) {
	d := Directory{
		FullPath: "./wrongDir",
	}
	err := d.GetFiles()
	assert.NotNil(t, err)
	assert.Equal(t, 0, len(d.Files))
}

func TestConcatenateFilesContent(t *testing.T) {
	d := Directory{
		FullPath: "./testdata",
	}
	d.GetFiles()
	content := d.ConcatenateFilesContent()
	assert.Contains(t, content, "A sample content")
	assert.Contains(t, content, "hello_world_with_no_desc")
	assert.Contains(t, content, "hello_world_with_blank")
}

func TestFormatHelps(t *testing.T) {
	d := Directory{
		FullPath: "./testdata",
	}
	d.GetFiles()
	content := d.FormatHelps()
	expected1 := fmt.Sprintf(formatedHelpTitle, "file1")
	expected2 := fmt.Sprintf(formatedHelpTitle, "file3")
	assert.Contains(t, content, expected1)
	assert.Contains(t, content, expected2)
}

func TestFilesFunctionsToCommands(t *testing.T) {
	d := Directory{
		FullPath: "./testdata",
	}
	d.GetFiles()
	c := d.FilesFunctionsToCommands()
	assert.Equal(t, "hello_world", c[0].Name)
	assert.Equal(t, "file3", c[0].Category)
	assert.Equal(t, "A target to display \"Hello, world!\" for function signature with multiline comment.", c[0].Usage)
	assert.Equal(t, "hello_world_with_blank", c[1].Name)
	assert.Equal(t, "file3", c[1].Category)
	assert.Equal(t, "A target to display \"Hello, world!\" with blank space for function signature with multiline comment.", c[1].Usage)
}
