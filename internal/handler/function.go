/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"strings"

	"github.com/urfave/cli/v2"
)

// formatedHelpLine Line formated for help function
const formatedHelpLine = "\033[0;32m  %-30s\033[0m %s\n"

// Function function structure
type Function struct {
	Name        string
	Description []string
	IsOverrided bool
}

// HasDescription Return true if function has at least a description
func (f *Function) HasDescription() bool {

	return len(f.Description) > 0
}

// Copy create a new function from a previous one
func (f *Function) Copy() *Function {

	newFunction := new(Function)
	newFunction.Name = f.Name
	newFunction.Description = make([]string, len(f.Description))
	copy(newFunction.Description, f.Description)

	return newFunction
}

// Reset errase data of the function
func (f *Function) Reset() {

	f.Name = ""
	f.Description = nil
	f.IsOverrided = false
}

// FormatHelp Format help of function
func (f *Function) FormatHelp() string {

	name := f.Name
	if f.IsOverrided {
		name = "(" + f.Name + ")"
	}

	helps := []string{fmt.Sprintf(formatedHelpLine, name, f.Description[0])}
	if len(f.Description) > 1 {
		for i := 1; i < len(f.Description); i++ {
			helps = append(helps, fmt.Sprintf(formatedHelpLine, " ", f.Description[i]))
		}
	}
	return strings.Join(helps, "")
}

// concatenateDescription Concatenate descrition in multi-line format
func (f *Function) concatenateDescription() string {

	var multiline, sep string
	for _, line := range f.Description {
		multiline = multiline + sep + line
		sep = " "
	}
	return multiline
}

// toCommand convert a function to a cli command
func (f *Function) toCommand(fileName string, content string) cli.Command {

	return cli.Command{
		Name:     f.Name,
		Category: fileName,
		Usage:    f.concatenateDescription(),
		Action: func(c *cli.Context) error {
			contentCommand := content + "\n" + f.Name + "\n"
			cmd := exec.Command("/bin/sh", "-c", contentCommand)
			cmd.Stdin = os.Stdin
			cmd.Stdout = os.Stdout
			cmd.Stderr = os.Stderr
			cmd.Env = os.Environ()
			err := cmd.Run()

			if err != nil {
				log.Fatal(err)
			}
			return nil
		},
	}
}
