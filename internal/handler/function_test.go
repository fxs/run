/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHasDescriptionTrue(t *testing.T) {
	f := Function{
		Name:        "",
		Description: []string{"a description"},
	}
	assert.Equal(t, true, f.HasDescription())
}

func TestHasDescriptionFalse(t *testing.T) {
	f := Function{}
	assert.Equal(t, false, f.HasDescription())
}

func TestCopy(t *testing.T) {
	f := Function{
		Name:        "a name",
		Description: []string{"a description", "another description"},
	}
	c := *f.Copy()
	assert.Equal(t, f, c)
}

func TestReset(t *testing.T) {
	f := Function{
		Name:        "a name",
		Description: []string{"a description", "another description"},
	}
	f.Reset()
	assert.Equal(t, "", f.Name)
	assert.Nil(t, f.Description)
}

func TestFormatHelpOneLine(t *testing.T) {
	name := "a name"
	desc := []string{"a description"}
	f := Function{
		Name:        name,
		Description: desc,
	}
	expected := fmt.Sprintf(formatedHelpLine, name, desc[0])
	assert.Equal(t, expected, f.FormatHelp())
}

func TestFormatHelpMultiLine(t *testing.T) {
	name := "a name"
	desc := []string{"a description", "another description"}
	f := Function{
		Name:        name,
		Description: desc,
	}
	expected := fmt.Sprintf(formatedHelpLine+formatedHelpLine, name, desc[0], "", desc[1])
	assert.Equal(t, expected, f.FormatHelp())
}

func TestFormatHelpOverrided(t *testing.T) {
	name := "a name"
	desc := []string{"a description"}
	f := Function{
		Name:        name,
		Description: desc,
		IsOverrided: true,
	}
	expected := fmt.Sprintf(formatedHelpLine, "("+name+")", desc[0])
	assert.Equal(t, expected, f.FormatHelp())
}

func TestConcatenateDescriptionOneLine(t *testing.T) {
	f := Function{
		Name:        "",
		Description: []string{"a description"},
	}
	assert.Equal(t, "a description", f.concatenateDescription())
}

func TestConcatenateDescriptionMultiLine(t *testing.T) {
	f := Function{
		Name:        "",
		Description: []string{"a description", "another description"},
	}
	assert.Equal(t, "a description another description", f.concatenateDescription())
}

func TestToCommand(t *testing.T) {
	name := "a name"
	desc := []string{"a description", "another description"}
	f := Function{
		Name:        name,
		Description: desc,
	}
	filename := "fileName"
	command := f.toCommand("fileName", "Content")

	assert.Equal(t, name, command.Name)
	assert.Equal(t, filename, command.Category)
	assert.Equal(t, "a description another description", command.Usage)
}
