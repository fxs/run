/*
run is a tool to unify build and deploy scripts execution.

Copyright (c) 2020 FX Soubirou

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package handler

import (
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/urfave/cli/v2"
)

// runDir Default directory of the run scripts
var defaultRunDir string = ".run.d"

// Directory directory structure
type Directory struct {
	FullPath string
	Files    []*File
}

// NewDirectory directory default constructor
func NewDirectory() (*Directory, error) {
	d := new(Directory)
	dir, err := os.Getwd()
	if err == nil {
		d.FullPath = dir + "/" + defaultRunDir
	}
	return d, err
}

// NewDirectoryWithPath directory default constructor
func NewDirectoryWithPath(p string) *Directory {
	d := new(Directory)
	d.FullPath = p
	return d
}

// IsExist check if directory exists and return true or false with the error
func (d *Directory) IsExist() (bool, error) {

	if _, err := os.Stat(d.FullPath); os.IsNotExist(err) {
		return false, err
	}
	return true, nil
}

// GetFiles get files list from directory return error if any or nil
func (d *Directory) GetFiles() error {

	files, err := ioutil.ReadDir(d.FullPath)
	if err != nil {
		return err
	}
	for _, f := range files {
		if !f.IsDir() {
			if file, er := NewFile(d.FullPath + "/" + f.Name()); er == nil {
				d.Files = append(d.Files, file)
			}
		}
	}
	d.SetOverridedFunction()
	return nil
}

// ConcatenateFilesContent concatenate all files content
func (d *Directory) ConcatenateFilesContent() string {

	contents := []string{}
	for _, file := range d.Files {
		contents = append(contents, file.Content)
	}

	return strings.Join(contents, "\n")
}

// FormatHelps Format all functions helps
func (d *Directory) FormatHelps() string {

	helps := []string{}
	for _, file := range d.Files {
		helps = append(helps, fmt.Sprintln(file.FormatHelps()))
	}
	return strings.Join(helps, "")
}

// FilesFunctionsToCommands convert for all files functions to cli commands
func (d *Directory) FilesFunctionsToCommands() []*cli.Command {

	commands := []*cli.Command{}
	for _, file := range d.Files {
		commands = append(commands, file.FunctionsToCommands(d.ConcatenateFilesContent())...)
	}
	return commands
}

// SetOverridedFunction Set if functions are overrided
func (d *Directory) SetOverridedFunction() {

	var functions []*Function
	for _, file := range d.Files {
		functions = append(functions, file.Functions...)
	}
	for i := 0; i < (len(functions) - 1); i++ {
		for j := (i + 1); j < len(functions); j++ {
			if functions[i].Name == functions[j].Name {
				functions[i].IsOverrided = true
				break
			}
		}
	}
}
